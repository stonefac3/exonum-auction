//! Exonum Auction models.
use chrono::{DateTime, Utc};
use exonum::crypto::{Hash, PublicKey};

// Users
//------------------------------------------------------------
encoding_struct! {
    /// User structure.
    struct User {
        /// Public key.
        public_key: &PublicKey,
        /// User name.
        name: &str,
        /// Current balance.
        balance: u64,
        /// Reserved money for the auction.
        reserved: u64,
    }
}

// Auction Lots
//------------------------------------------------------------
encoding_struct! {
    /// Auction Lot base info.
    struct LotInfo {
        /// Lot item title.
        title: &str,
        /// Lot description
        desc: &str,
    }
}
encoding_struct! {
    /// Auction Lot.
    struct Lot {
    	/// Lot identifier.
    	id: u64,
        /// Lot.
        lot_info: LotInfo,
        /// Current lot owner
        owner: &PublicKey,
    }
}

// Auctions
//------------------------------------------------------------
encoding_struct! {
    /// Auction base info.
    struct AuctionInfo {
        /// Auctioneer identifier.
        auctioneer: &PublicKey,
        /// Auction lot identifier.
        lot_id: u64,
        /// Start price.
        start_price: u64,
    }
}
encoding_struct! {
    /// Auction.
    struct Auction {
        /// Auction identifier.
        id: u64,
        /// Auction information.
        auction_info: AuctionInfo,
        /// Start time of the auction.
        started_at: DateTime<Utc>,
        /// Merkle root of history of bids. Last bid wins.
        bidding_merkle_root: &Hash,
        /// If closed then no more bids are accepted.
        closed: bool,
    }
}

// Auction Bids
//------------------------------------------------------------
encoding_struct! {
    /// Auction bid.
    struct Bid {
        /// Bidder.
        bidder: &PublicKey,
        /// Bid value.
        value: u64,
    }
}
