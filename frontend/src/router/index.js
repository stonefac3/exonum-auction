import Vue from 'vue'
import Router from 'vue-router'
import AuthPage from '../pages/Auth.vue'
import DashboardPage from '../pages/Dashboard.vue'
import UsersPage from '../pages/Users.vue'
import UserPage from '../pages/User.vue'
import LotsPage from '../pages/Lots.vue'
import LotPage from '../pages/Lot.vue'
import AuctionsPage from '../pages/Auctions.vue'
import AuctionPage from '../pages/Auction.vue'
import BlockchainPage from '../pages/Blockchain.vue'
import BlockPage from '../pages/Block.vue'
import TransactionPage from '../pages/Transaction.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'auth',
      component: AuthPage
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardPage
    },
    {
      path: '/users',
      name: 'users',
      component: UsersPage
    },
    {
      path: '/user/:publicKey',
      name: 'user',
      component: UserPage,
      props: true
    },
    {
      path: '/lots',
      name: 'lots',
      component: LotsPage
    },
    {
      path: '/lot/:id',
      name: 'lot',
      component: LotPage,
      props: true
    },
    {
      path: '/auctions',
      name: 'auctions',
      component: AuctionsPage
    },
    {
      path: '/auction/:id',
      name: 'auction',
      component: AuctionPage,
      props: true
    },
    {
      path: '/blockchain',
      name: 'blockchain',
      component: BlockchainPage
    },
    {
      path: '/block/:height',
      name: 'block',
      component: BlockPage,
      props: true
    },
    {
      path: '/transaction/:hash',
      name: 'transaction',
      component: TransactionPage,
      props: true
    }
  ]
})